using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Bookkeeper.Backend;
using SQLite;
using Android.Graphics;


namespace Bookkeeper
{
    [Activity(Label = "DetailedViewActivity")]
    public class DetailedViewActivity : Activity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.activity_detailed_view);

            int id = Intent.GetIntExtra("id", -1);
            Entry e = BookkeeperManager.Instance.GetEntry(id);

            TextView tv = FindViewById<TextView>(Resource.Id.detailed_text);
            ImageView iv = FindViewById<ImageView>(Resource.Id.picture_view);
            tv.Text = "Datum: "+e.Date + "\n";
            tv.Text += "Beskrivning: " + e.Description + "\n";
            if (e.Income)
            {
                tv.Text += "Inkomst-konto: " + e.Type + "\n";
                tv.Text += "Till konto: " + e.Account + "\n";
            }
            if (e.Expense)
            {
                tv.Text += "Utgifts-konto: " + e.Type + "\n";
                tv.Text += "Fr�n konto: " + e.Account + "\n";
            }
            tv.Text += "Total summa: " + e.Amount + " kr" + "\n";
            tv.Text += "Summa (exkl. moms): " + e.SalesAmount + " kr" + "\n";
            tv.Text += "Total moms: " + e.TaxAmount + " kr" + "\n";
            tv.Text += "Moms: " + e.TaxRate + " %";

			Bitmap bitmap = BitmapFactory.DecodeFile(e.ImagePath);
			iv.SetImageBitmap(bitmap);  
        }
    }
}