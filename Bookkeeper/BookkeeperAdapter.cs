using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using Bookkeeper.Backend;

namespace Bookkeeper
{
    public class BookkeeperAdapter : BaseAdapter
    {
        private Activity context;
        private List<Entry> entries;

        public BookkeeperAdapter(Activity activity, List<Entry> entries)
        {
            this.context = activity;
            this.entries = entries;
        }
        public override int Count
        {
            get { return entries.Count; }
        }

        public override Java.Lang.Object GetItem(int position)
        {
            return null;
        }

        public override long GetItemId(int position)
        {
            return entries[position].Id;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            View view;

            if (convertView == null)
            {
                view = context.LayoutInflater.Inflate
                (Resource.Layout.bookkeeper_list_item, parent, false);
            }
            else
            {
                view = convertView;
            }
            
            view.FindViewById<TextView>(Resource.Id.date).Text 
                = entries[position].Date;
            view.FindViewById<TextView>(Resource.Id.message).Text 
                = entries[position].Description;
            view.FindViewById<TextView>(Resource.Id.amount).Text 
                = entries[position].Amount+" kr";

            return view;

        }
    }
}