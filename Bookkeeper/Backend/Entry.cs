using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using SQLite;

namespace Bookkeeper.Backend
{
    public class Entry
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public string Date { get; set; }
        public string Description { get; set; }
        public int Amount { get; set; }
        public string Account { get; set; }
        public string Type { get; set; }
        public int TaxRate { get; set; }
        public bool Income { get; set; }
        public bool Expense { get; set; }
        public string ImagePath { get; set; }
        public double TaxAmount 
        { 
            get 
            {
                return (TaxRate * 0.01) * Amount;  
            } 
        }
        public double SalesAmount 
        { 
            get 
            {
                return Amount - TaxAmount;
            } 
        }
    }
}