using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using SQLite;
using Java.IO;

namespace Bookkeeper.Backend
{
    public class BookkeeperManager
    {
        private List<Entry> entryList = new List<Entry>();
        private List<Account> incomeAccounts = new List<Account>();
        private List<Account> expenseAccounts = new List<Account>();
        private List<Account> moneyAccounts = new List<Account>();
        private List<TaxRate> taxRates = new List<TaxRate>();
        private string path = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal) + "\\database.db";
        
        private BookkeeperManager()
        {
            SQLiteConnection db = new SQLiteConnection(path);

            db.CreateTable<TaxRate>();
            db.CreateTable<Account>();
            db.CreateTable<Entry>();
            

            if (db.Table<Account>().Count() == 0)
            {
                db.Insert(new Account { Name = "F�rs�ljning", Number = 3000 });
                db.Insert(new Account { Name = "F�rs�ljning av tj�nster", Number = 3040 });
                db.Insert(new Account { Name = "F�rbrukningsinventarier och f�rbrukningsmaterial", Number = 5400 });
                db.Insert(new Account { Name = "�vriga egna uttag", Number = 2013 });
                db.Insert(new Account { Name = "Reklam och PR", Number = 5900 });
                db.Insert(new Account { Name = "Kassa", Number = 1910 });
                db.Insert(new Account { Name = "F�retagskonto", Number = 1930 });
                db.Insert(new Account { Name = "Egna ins�ttningar", Number = 2018 });

                db.Insert(new TaxRate { Tax = 6 });
                db.Insert(new TaxRate { Tax = 12 });
                db.Insert(new TaxRate { Tax = 25 });

                db.Close();
            }
        }

        private static BookkeeperManager instance;
        public static BookkeeperManager Instance {
            get
            {
                if (instance == null)
                {
                    instance = new BookkeeperManager();
                }
                return instance;
            }
        }
        
        public List<Entry> EntryList
        {
            get
            {
                SQLiteConnection db = new SQLiteConnection(path);
                var a = db.Table<Entry>().OrderBy(i => i.Date);

                return a.ToList();
            }
        }
        public List<Account> IncomeAccounts
        {
            get
            {
                SQLiteConnection db = new SQLiteConnection(path);
                var a = db.Table<Account>().Where(i => i.Number > 2999 && i.Number < 3041);

                return a.ToList();
            }
        }
        public List<Account> ExpenseAccounts
        {
            get
            {
                SQLiteConnection db = new SQLiteConnection(path);
                var a = db.Table<Account>().Where(i => i.Number > 5399 || i.Number == 2013);

                return a.ToList();
            }
        }
        public List<Account> MoneyAccounts 
        {
            get 
            {
                SQLiteConnection db = new SQLiteConnection(path);
                var a = db.Table<Account>().Where(i => i.Number < 1931 || i.Number == 2018);
                
                return a.ToList(); 
            } 
        }
        public List<TaxRate> TaxRates
        {
            get
            {
                SQLiteConnection db = new SQLiteConnection(path);
                return db.Table<TaxRate>().ToList();

            }
        }
        public void AddEntry(Entry e)
        {
            SQLiteConnection db = new SQLiteConnection(path);
            db.Insert(e);
            db.Close();
        }

        public Entry GetEntry(int id) 
        {
            SQLiteConnection db = new SQLiteConnection(path);
            return db.Get<Entry>(id);
        }
    }
}