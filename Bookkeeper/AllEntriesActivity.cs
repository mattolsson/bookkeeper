using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using Bookkeeper.Backend;

namespace Bookkeeper
{
    [Activity(Label = "AllEntriesActivity")]
    public class AllEntriesActivity : Activity
    {
        private ListView entryList;
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView(Resource.Layout.activity_all_entries);
            
            entryList = FindViewById<ListView>(Resource.Id.entry_list);
            entryList.Adapter = new BookkeeperAdapter(this, BookkeeperManager.Instance.EntryList);

            entryList.ItemClick += listItem_DetailedEntryView;
        }

        private void listItem_DetailedEntryView(object sender, AdapterView.ItemClickEventArgs e)
        {
            int id = (int)entryList.Adapter.GetItemId(e.Position);
            Intent intent = new Intent(this, typeof(DetailedViewActivity));
            intent.PutExtra("id", id);
            StartActivity(intent);
        }
    }
}