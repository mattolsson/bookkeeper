using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using Bookkeeper.Backend;
using SQLite;
using AFile = Java.IO.File;
using AUri = Android.Net.Uri;
using AEnvironment = Android.OS.Environment;
using Java.IO;
using Android.Provider;

namespace Bookkeeper
{
    [Activity(Label = "NewEntryActivity")]
    public class NewEntryActivity : Activity
    {
        private RadioButton incomeRadio, expenseRadio;
        private EditText dateText, descriptionText, amountText;
        private Button madeEntryButton, takeImageButton;
        private bool income = false;
        private bool expense = false;
        private Spinner otherAccountSpinner, ownAccountSpinner, taxRateSpinner;
        private string accountSpinnerSelected, typeSpinnerSelected, taxSpinnerSelected;
        private AUri myUri;
		private Entry entry;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView(Resource.Layout.activity_new_entry);

            incomeRadio = FindViewById<RadioButton>(Resource.Id.income_radio);
            expenseRadio = FindViewById<RadioButton>(Resource.Id.expense_radio);
            
            incomeRadio.Click += radiobutton_IncomeVsExpense;
            expenseRadio.Click += radiobutton_IncomeVsExpense;

            dateText = FindViewById<EditText>(Resource.Id.date_edit);
            descriptionText = FindViewById<EditText>(Resource.Id.description_edit);
            amountText = FindViewById<EditText>(Resource.Id.amount_edit);
            
            madeEntryButton = FindViewById<Button>(Resource.Id.made_entry_button);
            takeImageButton = FindViewById<Button>(Resource.Id.take_image_button);


            List<string> moneySpinner = BookkeeperManager.Instance.MoneyAccounts.Select(i => i.Name + " (" + i.Number + ")").ToList();
            ownAccountSpinner = FindViewById<Spinner>(Resource.Id.own_account_spinner);
            ArrayAdapter ownAccountAdapter = new ArrayAdapter(this, Android.Resource.Layout.SimpleSpinnerItem, moneySpinner);
            ownAccountAdapter.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);
            ownAccountSpinner.Adapter = ownAccountAdapter;
            ownAccountSpinner.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs>(accountSpinner_Selected);

            List<string> taxSpinner = BookkeeperManager.Instance.TaxRates.Select(i => i.Tax + "").ToList();
            taxRateSpinner = FindViewById<Spinner>(Resource.Id.tax_rate_spinner);
            ArrayAdapter taxRateAdapter = new ArrayAdapter(this, Android.Resource.Layout.SimpleSpinnerItem, taxSpinner);
            taxRateAdapter.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);
            taxRateSpinner.Adapter = taxRateAdapter;
            taxRateSpinner.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs>(taxSpinner_Selected);

            madeEntryButton.Click += SaveEntryData;
            takeImageButton.Click += takeImage_Clicked;
 
        }

        private void taxSpinner_Selected(object sender, AdapterView.ItemSelectedEventArgs e)
        {
            Spinner spinner = (Spinner)sender;
            taxSpinnerSelected = spinner.SelectedItem.ToString();
        }

        private void accountSpinner_Selected(object sender, AdapterView.ItemSelectedEventArgs e)
        {
            Spinner spinner = (Spinner)sender;
            accountSpinnerSelected = spinner.SelectedItem.ToString();
        }

        private void typeSpinner_Selected(object sender, AdapterView.ItemSelectedEventArgs e)
        {
            Spinner spinner = (Spinner)sender;
            typeSpinnerSelected = spinner.SelectedItem.ToString();
        }

        private void takeImage_Clicked(object sender, EventArgs e)
        {   
			entry = new Entry();
            Intent intent = new Intent(MediaStore.ActionImageCapture);
            File picDir = AEnvironment.GetExternalStoragePublicDirectory(AEnvironment.DirectoryPictures);
            File myDir = new File(picDir, "EntryPics");
            
            if (!myDir.Exists())
            {
                myDir.Mkdirs();
            }

			File myFile = new File(myDir, "entry"+(BookkeeperManager.Instance.EntryList.Count+1)+".jpg");
            myUri = AUri.FromFile(myFile);
            intent.PutExtra(MediaStore.ExtraOutput, myUri);
            StartActivityForResult(intent, 0);
        }
        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            ImageView imageView = FindViewById<ImageView>(Resource.Id.image_view);

            if (requestCode == 0 && resultCode == Result.Ok)
            {
                int height = Resources.DisplayMetrics.HeightPixels;
                int width = imageView.Width;
                Bitmap bitmap = ImageUtils.LoadAndScaleBitmap(myUri.Path, width, height);
                imageView.SetImageBitmap(bitmap);
            }
            else
            {
                base.OnActivityResult(requestCode, resultCode, data);
            }
        }

        private void SaveEntryData(object sender, EventArgs e)
        {
            string date, description, account, type, imagePath;
            int amount, taxRate;
            date = dateText.Text;
            description = descriptionText.Text;
            amount = int.Parse(amountText.Text);
            taxRate = int.Parse(taxSpinnerSelected);
            account = accountSpinnerSelected;
            type = typeSpinnerSelected;
			imagePath = myUri.Path;

			entry.Date = date;
			entry.Description = description;
			entry.Amount = amount;
			entry.Account = account;
			entry.TaxRate = taxRate;
			entry.Type = type;
			entry.Income = income;
			entry.Expense = expense;
			entry.ImagePath = imagePath;
            
            BookkeeperManager.Instance.AddEntry(entry);

            Intent intent = new Intent(this, typeof(AllEntriesActivity));
            StartActivity(intent);
        }

        
        private void radiobutton_IncomeVsExpense(object sender, EventArgs e)
        {
            if (incomeRadio.Checked)
            {
                income = true;
                expense = false;
                List<string> incomeSpinner = BookkeeperManager.Instance.IncomeAccounts.Select(i => i.Name + " (" + i.Number + ")").ToList();
                otherAccountSpinner = FindViewById<Spinner>(Resource.Id.other_account_spinner);
                ArrayAdapter otherAccountAdapter = new ArrayAdapter(this, Android.Resource.Layout.SimpleSpinnerItem, incomeSpinner);
                otherAccountAdapter.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);
                otherAccountSpinner.Adapter = otherAccountAdapter;
                otherAccountSpinner.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs>(typeSpinner_Selected);
            }
            else if(expenseRadio.Checked)
            {
                expense = true;
                income = false;
                List<string> expenseSpinner = BookkeeperManager.Instance.ExpenseAccounts.Select(i => i.Name + " (" + i.Number + ")").ToList();
                otherAccountSpinner = FindViewById<Spinner>(Resource.Id.other_account_spinner);
                ArrayAdapter otherAccountAdapter2 = new ArrayAdapter(this, Android.Resource.Layout.SimpleSpinnerItem, expenseSpinner);
                otherAccountAdapter2.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);
                otherAccountSpinner.Adapter = otherAccountAdapter2;
                otherAccountSpinner.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs>(typeSpinner_Selected);
            }
        }
    }
}