﻿using System;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;

using Bookkeeper.Backend;
namespace Bookkeeper
{
    [Activity(Label = "Bookkeeper", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity
    {
        

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.activity_main);

            // Get our button from the layout resource,
            // and attach an event to it
            FindViewById<Button>(Resource.Id.newEvent).Click += delegate {
                Intent intent = new Intent(this, typeof(NewEntryActivity));
                StartActivity(intent);
            };
            FindViewById<Button>(Resource.Id.allEvents).Click += delegate {
                Intent intent = new Intent(this, typeof(AllEntriesActivity));
                StartActivity(intent);
            };
            FindViewById<Button>(Resource.Id.report).Click += delegate
            {
                Intent intent = new Intent(this, typeof(ReportActivity));
                StartActivity(intent);
            };
            
            
        }
    }
}

